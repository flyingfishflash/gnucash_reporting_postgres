﻿-- BEGIN When Deleting a Lot
2015-10-10 09:55:21 EDTLOG:  statement: BEGIN
2015-10-10 09:55:21 EDTLOG:  statement: UPDATE lots SET account_guid='d52ba04ccb2adda7d8d4fbbe2026e9d7',is_closed=1 WHERE guid = 'd858d464624c0153acda7d03ba84e926'
2015-10-10 09:55:21 EDTLOG:  statement: SELECT * FROM slots WHERE obj_guid='d858d464624c0153acda7d03ba84e926' and slot_type in ('9', '8') and not guid_val is null
2015-10-10 09:55:21 EDTLOG:  statement: DELETE FROM slots  WHERE obj_guid = 'd858d464624c0153acda7d03ba84e926'
2015-10-10 09:55:21 EDTLOG:  statement: INSERT INTO slots(obj_guid,name,slot_type,int64_val,string_val,double_val,timespec_val,guid_val,numeric_val_num,numeric_val_denom,gdate_val) VALUES('d858d464624c0153acda7d03ba84e926','title',4,0,'Lot 0',0,NULL,NULL,0,1,NULL)
2015-10-10 09:55:21 EDTLOG:  statement: INSERT INTO slots(obj_guid,name,slot_type,int64_val,string_val,double_val,timespec_val,guid_val,numeric_val_num,numeric_val_denom,gdate_val) VALUES('d858d464624c0153acda7d03ba84e926','notes',4,0,'',0,NULL,NULL,0,1,NULL)
2015-10-10 09:55:21 EDTLOG:  statement: COMMIT
2015-10-10 09:55:21 EDTLOG:  statement: BEGIN
2015-10-10 09:55:21 EDTLOG:  statement: DELETE FROM lots  WHERE guid = 'd858d464624c0153acda7d03ba84e926'
2015-10-10 09:55:21 EDTLOG:  statement: SELECT * FROM slots WHERE obj_guid='d858d464624c0153acda7d03ba84e926' and slot_type in ('9', '8') and not guid_val is null
2015-10-10 09:55:21 EDTLOG:  statement: DELETE FROM slots  WHERE obj_guid = 'd858d464624c0153acda7d03ba84e926'
2015-10-10 09:55:21 EDTLOG:  statement: COMMIT
-- END When Deleting a Lot

-- BEGIN DELETE Realized Gain transaction
2015-10-10 10:07:57 EDTLOG:  statement: BEGIN
2015-10-10 10:07:57 EDTLOG:  statement: DELETE FROM transactions  WHERE guid = '56a55114a59ec2748e807ab4dc72c9a6'
2015-10-10 10:07:57 EDTLOG:  statement: SELECT * FROM slots WHERE obj_guid='56a55114a59ec2748e807ab4dc72c9a6' and slot_type in ('9', '8') and not guid_val is null
2015-10-10 10:07:57 EDTLOG:  statement: DELETE FROM slots  WHERE obj_guid = '56a55114a59ec2748e807ab4dc72c9a6'
2015-10-10 10:07:57 EDTLOG:  statement: DELETE FROM splits  WHERE tx_guid = '56a55114a59ec2748e807ab4dc72c9a6'
2015-10-10 10:07:57 EDTLOG:  statement: SELECT * FROM slots WHERE obj_guid='950fc8945983d71dc5d1621162be1295' and slot_type in ('9', '8') and not guid_val is null
2015-10-10 10:07:57 EDTLOG:  statement: DELETE FROM slots  WHERE obj_guid = '950fc8945983d71dc5d1621162be1295'
2015-10-10 10:07:57 EDTLOG:  statement: SELECT * FROM slots WHERE obj_guid='923aa9f09df10279f46fd7f39f873e39' and slot_type in ('9', '8') and not guid_val is null
2015-10-10 10:07:57 EDTLOG:  statement: DELETE FROM slots  WHERE obj_guid = '923aa9f09df10279f46fd7f39f873e39'
2015-10-10 10:07:57 EDTLOG:  statement: COMMIT
2015-10-10 10:07:57 EDTLOG:  statement: BEGIN
2015-10-10 10:07:57 EDTLOG:  statement: DELETE FROM splits  WHERE guid = '950fc8945983d71dc5d1621162be1295'
2015-10-10 10:07:57 EDTLOG:  statement: COMMIT
2015-10-10 10:07:57 EDTLOG:  statement: BEGIN
2015-10-10 10:07:57 EDTLOG:  statement: DELETE FROM splits  WHERE guid = '923aa9f09df10279f46fd7f39f873e39'
2015-10-10 10:07:57 EDTLOG:  statement: COMMIT
-- END Delete Realized Gain Transaction

-- Find GUID of Orphaned gains account
-- Find GUID of Realized Gains account

-- Reassign Realized Gain account from Orphaned
2015-10-10 10:20:20 EDTLOG:  statement: BEGIN
2015-10-10 10:20:20 EDTLOG:  statement: UPDATE splits SET tx_guid='53679990137fcb8c6fa3c25326bae6dd',account_guid='a5d747e7ba2191a2015af8c0dca6d5b5',memo='Realized Gain/Loss',action='',reconcile_state='n',reconcile_date=NULL,value_num=-79192,value_denom=100,quantity_num=-79192,quantity_denom=100,lot_guid=NULL WHERE guid = '42f4d7a199de22429a1fe315a3e3d28e'
2015-10-10 10:20:20 EDTLOG:  statement: SELECT * FROM slots WHERE obj_guid='42f4d7a199de22429a1fe315a3e3d28e' and slot_type in ('9', '8') and not guid_val is null
2015-10-10 10:20:20 EDTLOG:  statement: DELETE FROM slots  WHERE obj_guid = '42f4d7a199de22429a1fe315a3e3d28e'
2015-10-10 10:20:20 EDTLOG:  statement: COMMIT
-- Reassign Realized Gain account from Orphaned
