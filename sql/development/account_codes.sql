﻿select
intid
, guid
--, name
, longname
, account_type
, code
, substring(code from 3 for 2)
, replace(substring(code from 1 for 4), '09', '19') || substring(code from 5)
from reporting.accounts
where code like '4 09%'

-- run in production
update
accounts
set code = replace(substring(code from 1 for 4), '17', '07') || substring(code from 5)
where code like '4 17%'

--select dba.fn_refresh_reporting()