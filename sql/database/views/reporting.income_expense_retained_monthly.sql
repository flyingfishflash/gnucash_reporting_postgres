-- View: reporting.income_expense_retained_monthly

-- DROP VIEW reporting.income_expense_retained_monthly;

CREATE OR REPLACE VIEW reporting.income_expense_retained_monthly AS 

WITH cte_splits AS (

SELECT
	reporting.fns_lastdaymonth(s.post_date::date) AS post_monthenddate
	, a.account_class
	, s.quantity
	, t.tax_expense
	, h.housing_expense
	, a.longname

FROM reporting.splits s
	JOIN reporting.accounts a
		ON s.account_guid::text = a.guid::text
	LEFT JOIN reporting.accounts_expense_tax t
		ON s.account_guid::text = t.guid::text
	LEFT JOIN reporting.accounts_expense_housing h
		ON s.account_guid::text = h.guid::text

WHERE
	a.account_class::text = 'INCOME'::text
	OR a.account_class::text = 'EXPENSE'::text

),

cte_income AS (

SELECT
	s.post_monthenddate
	, sum(s.quantity) AS income
	
FROM cte_splits s

WHERE
	s.account_class::text = 'INCOME'::text

GROUP BY
	s.post_monthenddate

),

cte_income_wagestakehome AS (

SELECT
	s.post_monthenddate
	, sum(s.quantity) AS incomewagestakehome

FROM cte_splits s

WHERE
	s.account_class::text = 'INCOME'::text
	AND s.longname::text ~~ '%Wages - Take Home%'::text

GROUP BY
	s.post_monthenddate

),

cte_expense AS (
SELECT
	s.post_monthenddate
	, sum(s.quantity) AS expense

FROM cte_splits s

WHERE
	s.account_class::text = 'EXPENSE'::text

GROUP BY
	s.post_monthenddate

),

cte_expenseaftertax AS (

SELECT
	s.post_monthenddate
	, sum(s.quantity) AS expenseaftertax

FROM cte_splits s

WHERE
	s.account_class::text = 'EXPENSE'::text
	AND s.tax_expense IS NULL

GROUP BY
	s.post_monthenddate

), 

cte_expensehousing AS (

SELECT
	s.post_monthenddate
	, sum(s.quantity) AS expensehousing

FROM cte_splits s

WHERE
	s.account_class::text = 'EXPENSE'::text
	AND s.housing_expense IS NOT NULL

GROUP BY
	s.post_monthenddate

)

SELECT
	date_part('year'::text, m.monthenddate)::integer AS year
	, m.monthenddate AS month
	, - COALESCE(cte_income.income, 0::numeric) AS income
	, - COALESCE(cte_income_wagestakehome.incomewagestakehome, 0::numeric) AS incomewagestakehome
	, COALESCE(cte_expense.expense, 0::numeric) AS expense
	, COALESCE(cte_expenseaftertax.expenseaftertax, 0::numeric) AS expenseaftertax
	, COALESCE(cte_expensehousing.expensehousing, 0::numeric) AS expensehousing
	, - (COALESCE(cte_income.income, 0::numeric) + COALESCE(cte_expense.expense, 0::numeric)) AS retained
	, - (COALESCE(cte_income_wagestakehome.incomewagestakehome, 0::numeric) + COALESCE(cte_expenseaftertax.expenseaftertax, 0::numeric)) AS retainedtakehome
	, COALESCE((COALESCE(cte_income.income, 0::numeric) + COALESCE(cte_expense.expense, 0::numeric)) / cte_income.income, 0::numeric) AS retainedpercentage
	, COALESCE((COALESCE(cte_income_wagestakehome.incomewagestakehome, 0::numeric) + COALESCE(cte_expenseaftertax.expenseaftertax, 0::numeric)) / cte_income_wagestakehome.incomewagestakehome, 0::numeric) AS retainedpercentagetakehome

FROM reporting.dateseries_monthend m
	LEFT JOIN cte_income
		ON m.monthenddate = cte_income.post_monthenddate
	LEFT JOIN cte_income_wagestakehome
		ON m.monthenddate = cte_income_wagestakehome.post_monthenddate
	LEFT JOIN cte_expense
		ON m.monthenddate = cte_expense.post_monthenddate
	LEFT JOIN cte_expenseaftertax
		ON m.monthenddate = cte_expenseaftertax.post_monthenddate
	LEFT JOIN cte_expensehousing
		ON m.monthenddate = cte_expensehousing.post_monthenddate;
	
