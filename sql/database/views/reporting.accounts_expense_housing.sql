-- View: reporting.accounts_expense_housing

--DROP VIEW reporting.accounts_expense_housing;

CREATE OR REPLACE VIEW reporting.accounts_expense_housing AS 

SELECT
	da.intid
	, da.guid
	, da.name
	, da.longname
	, a.rgt
	, da.lft
	, da.depth
	, 1::bit(1) AS housing_expense

FROM reporting.accounts a
     JOIN reporting.accounts da
		ON a.name IN ('Housing - Base', 'Housing - Base - Occupant Services')
		AND a.account_type::text = 'EXPENSE'::text
		AND da.lft >= a.lft AND da.lft <= a.rgt

ORDER BY
	da.lft;