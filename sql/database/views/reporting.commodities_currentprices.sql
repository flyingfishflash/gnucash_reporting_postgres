-- View: reporting.commodities_currentprices

-- DROP VIEW reporting.commodities_currentprices;

CREATE OR REPLACE VIEW reporting.commodities_currentprices AS 

WITH cte_maxpricedate AS (

SELECT
	p1_1.commodity_guid
	, max(p1_1.date) AS maxdate

FROM reporting.prices p1_1

WHERE
	p1_1.date::date <= 'now'::text::date

GROUP BY
	p1_1.commodity_guid

)

SELECT DISTINCT
    p1.commodity_guid
    , c.fullname
    , c.mnemonic
    , p1.date AS pricedate
    , p1.price

FROM reporting.prices p1
    JOIN cte_maxpricedate mpd
        ON p1.commodity_guid::text = mpd.commodity_guid::text AND p1.date = mpd.maxdate
    LEFT JOIN reporting.commodities c
        ON c.guid::text = p1.commodity_guid::text

ORDER BY
    p1.date DESC;
