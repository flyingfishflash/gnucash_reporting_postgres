-- View: reporting.portfolio_dividends_yearly

-- DROP VIEW reporting.portfolio_dividends_yearly;

CREATE OR REPLACE VIEW reporting.portfolio_dividends_yearly AS 

SELECT
	portfolio_dividends_quarterly.year
	, portfolio_dividends_quarterly.a_intid
	, portfolio_dividends_quarterly.account
	, sum(portfolio_dividends_quarterly.sum) AS sum

FROM reporting.portfolio_dividends_quarterly

GROUP BY
	portfolio_dividends_quarterly.a_intid
	, portfolio_dividends_quarterly.account
	, portfolio_dividends_quarterly.year

ORDER BY
	portfolio_dividends_quarterly.year
	, portfolio_dividends_quarterly.a_intid
	, portfolio_dividends_quarterly.account;
