-- View: reporting.income_expense_retained_average_monthly

-- DROP VIEW reporting.income_expense_retained_average_monthly;

CREATE OR REPLACE VIEW reporting.income_expense_retained_average_monthly AS 

SELECT
	income_expense_retained_monthly.year
	, income_expense_retained_monthly.month
	, income_expense_retained_monthly.income
	, income_expense_retained_monthly.expense
	, income_expense_retained_monthly.expenseaftertax
	, income_expense_retained_monthly.retained
	, income_expense_retained_monthly.retainedpercentage
	, avg(income_expense_retained_monthly.income) OVER (ORDER BY income_expense_retained_monthly.month) AS incomeaverage
	, avg(income_expense_retained_monthly.expense) OVER (ORDER BY income_expense_retained_monthly.month) AS expenseaverage
	, avg(income_expense_retained_monthly.expenseaftertax) OVER (ORDER BY income_expense_retained_monthly.month) AS expenseaftertaxaverage
	, avg(income_expense_retained_monthly.expensehousing) OVER (ORDER BY income_expense_retained_monthly.month) AS expensehousingaverage
	, avg(income_expense_retained_monthly.retained) OVER (ORDER BY income_expense_retained_monthly.month) AS retainedaverage
	, avg(income_expense_retained_monthly.retainedpercentage) OVER (ORDER BY income_expense_retained_monthly.month) AS retainedpercentageaverage

FROM reporting.income_expense_retained_monthly

ORDER BY
	income_expense_retained_monthly.month;
