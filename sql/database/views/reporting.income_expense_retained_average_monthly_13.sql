﻿-- View: reporting.income_expense_retained_average_monthly_13

-- DROP VIEW reporting.income_expense_retained_average_monthly_13;

CREATE OR REPLACE VIEW reporting.income_expense_retained_average_monthly_13 AS 

SELECT
	m.year
	, m.month
	, m.income
	, m.expense
	, m.expenseaftertax
	, m.retained
	, m.retainedpercentage
	, (select avg(a.income) from reporting.income_expense_retained_monthly a where a.month >= m.month::date - '1 year'::interval and a.month::date <= m.month ) as incomeaverage
	, (select avg(a.expense) from reporting.income_expense_retained_monthly a where a.month >= m.month::date - '1 year'::interval and a.month::date <= m.month ) as expenseaverage
	, (select avg(a.expenseaftertax) from reporting.income_expense_retained_monthly a where a.month >= m.month::date - '1 year'::interval and a.month::date <= m.month ) as expenseaftertaxaverage
	, (select avg(a.expensehousing) from reporting.income_expense_retained_monthly a where a.month >= m.month::date - '1 year'::interval and a.month::date <= m.month ) as expensehousingaverage
	, (select avg(a.retained) from reporting.income_expense_retained_monthly a where a.month >= m.month::date - '1 year'::interval and a.month::date <= m.month ) as retainedaverage
	, (select avg(a.retainedpercentage) from reporting.income_expense_retained_monthly a where a.month >= m.month::date - '1 year'::interval and a.month::date <= m.month ) as retainedpercentageaverage
	--, avg(m.income) OVER (ORDER BY m.month) AS incomeaverageo
	--, avg(m.expense) OVER (ORDER BY m.month) AS expenseaverageo
	--, avg(m.expenseaftertax) OVER (ORDER BY m.month) AS expenseaftertaxaverageo
	--, avg(m.expensehousing) OVER (ORDER BY m.month) AS expensehousingaverageo
	--, avg(m.retained) OVER (ORDER BY m.month) AS retainedaverageo
	--, avg(m.retainedpercentage) OVER (ORDER BY m.month) AS retainedpercentageaverageo

FROM reporting.income_expense_retained_monthly m

WHERE
	m.month >= (reporting.fns_lastdaymonth(now()::date) - '1 year'::interval)

ORDER BY
	m.month;
