-- View: reporting.income_expense_detail_monthly

-- DROP VIEW reporting.income_expense_detail_monthly;

CREATE OR REPLACE VIEW reporting.income_expense_detail_monthly AS 

WITH cte_splits AS (

SELECT
	reporting.fns_lastdaymonth(s.post_date::date) AS post_monthenddate
	, a_1.account_class
	, CASE
		WHEN a_1.depth > 4
		THEN ( SELECT a1.guid
			   FROM reporting.accounts a1
			   WHERE a1.lft < a_1.lft AND a1.depth = 4
			   ORDER BY a1.lft DESC
			   LIMIT 1)
		ELSE a_1.guid
	  END AS level4parent_a_guid
  , s.quantity

FROM reporting.splits s
	JOIN reporting.accounts a_1
		ON a_1.guid::text = s.account_guid::text

WHERE
	(a_1.account_class::text = 'INCOME'::text OR a_1.account_class::text = 'EXPENSE'::text)
	AND s.post_date::date > reporting.fns_lastdaymonth((reporting.fns_lastdaymonth(now()::date) - '3 mons'::interval)::date)

),

cte_splits_sum_bylevel4 AS (

SELECT
	cte_splits.post_monthenddate
	, cte_splits.account_class
	, cte_splits.level4parent_a_guid
	, sum(cte_splits.quantity) AS balance

FROM cte_splits

GROUP BY
	cte_splits.post_monthenddate
	, cte_splits.account_class
	, cte_splits.level4parent_a_guid

),

cte_monthenddates AS (

SELECT
	me_1.monthenddate
	, unnest(ARRAY['INCOME'::text, 'EXPENSE'::text])::character varying AS account_class

FROM reporting.dateseries_monthend me_1

WHERE
	me_1.monthenddate > reporting.fns_lastdaymonth((reporting.fns_lastdaymonth(now()::date) - '3 mons'::interval)::date)

)

SELECT
	me.monthenddate
	, me.account_class
	, a.intid AS level4parent_a_intid
	, a.name
	, l4.balance
	, l4.level4parent_a_guid

FROM cte_monthenddates me

LEFT JOIN cte_splits_sum_bylevel4 l4
	ON l4.post_monthenddate = me.monthenddate
	AND l4.account_class::text = me.account_class::text
LEFT JOIN reporting.accounts a
	ON a.guid::text = l4.level4parent_a_guid::text

ORDER BY
	me.monthenddate
	, a.intid;
