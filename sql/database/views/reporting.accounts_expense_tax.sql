-- View: reporting.accounts_expense_tax

-- DROP VIEW reporting.accounts_expense_tax;

CREATE OR REPLACE VIEW reporting.accounts_expense_tax AS 

SELECT
	accounts.intid
	, accounts.guid
	, accounts.account_type
	, accounts.name
	, 1::bit(1) AS tax_expense

FROM reporting.accounts

WHERE
	accounts.account_type::text = 'EXPENSE'::text
	AND accounts.name::text IN (
		  'Federal Income Tax Withheld'
		, 'State Income Tax Withheld'
		, 'Medicare Tax Withheld'
		, 'Social Security Tax Withheld'
		, 'Prior Year Tax Owed at Filing'
		, 'Federal Income Tax Owed'
		, 'State Income Tax Owed'
		, 'Prior Year Tax Over Payment Refund'
		, 'Federal Income Tax Refund'
		, 'State Income Tax Refund'
		, 'Income Tax'
		, 'Tax Payments from Wages')

ORDER BY
	accounts.name;
