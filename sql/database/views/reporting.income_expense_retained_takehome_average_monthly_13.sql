﻿-- View: reporting.income_expense_retained_takehome_average_monthly_13

-- DROP VIEW reporting.income_expense_retained_takehome_average_monthly_13;

CREATE OR REPLACE VIEW reporting.income_expense_retained_takehome_average_monthly_13 AS 

WITH takehome AS (

SELECT
	m.year
	, m.month
	, m.incomewagestakehome
	, m.expenseaftertax
	, m.retainedtakehome
	, m.retainedpercentagetakehome
	, (select avg(a.incomewagestakehome) from reporting.income_expense_retained_monthly a where a.month >= m.month::date - '1 year'::interval and a.month::date <= m.month ) as incomewagestakehomeaverage
	, (select avg(a.expenseaftertax) from reporting.income_expense_retained_monthly a where a.month >= m.month::date - '1 year'::interval and a.month::date <= m.month ) as expenseaftertaxaverage
	, (select avg(a.retainedtakehome) from reporting.income_expense_retained_monthly a where a.month >= m.month::date - '1 year'::interval and a.month::date <= m.month ) as retainedtakehomeaverage
-- 	, avg(m.incomewagestakehome) OVER (ORDER BY m.month) AS incomewagestakehomeaverageo
-- 	, avg(m.expenseaftertax) OVER (ORDER BY m.month) AS expenseaftertaxaverageo
-- 	, avg(m.retainedtakehome) OVER (ORDER BY m.month) AS retainedtakehomeaverageo

FROM reporting.income_expense_retained_monthly m

WHERE
	m.month >= (reporting.fns_lastdaymonth(now()::date) - '1 year'::interval)

)

SELECT
	takehome.year
	, takehome.month
	, takehome.incomewagestakehome
	, takehome.expenseaftertax
	, takehome.retainedtakehome
	, takehome.retainedpercentagetakehome
	, takehome.incomewagestakehomeaverage
	, takehome.expenseaftertaxaverage
	, takehome.retainedtakehomeaverage
	, takehome.retainedtakehomeaverage / takehome.incomewagestakehomeaverage AS retainedpercentagetakehomeaverage

FROM takehome

ORDER BY
	takehome.month;
