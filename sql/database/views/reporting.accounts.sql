-- Materialized View: reporting.accounts

-- DROP MATERIALIZED VIEW reporting.accounts;

CREATE MATERIALIZED VIEW reporting.accounts AS 

WITH RECURSIVE raccounts AS (

SELECT
    p.guid
    , p.name
    , p.account_type
    , p.commodity_guid
    , p.commodity_scu
    , p.non_std_scu
    , p.parent_guid
    , p.code
    , p.description
    , p.hidden
    , p.placeholder
    , p.name AS longname
    , 1 AS depth

FROM gnucash.accounts p
     LEFT JOIN reporting.commodities c_1
        ON p.commodity_guid::text = c_1.guid::text

WHERE
    p.parent_guid IS NULL
    AND p.name::text <> 'Template Root'::text

UNION ALL

SELECT
    p.guid
    , p.name
    , p.account_type
    , p.commodity_guid
    , p.commodity_scu
    , p.non_std_scu
    , p.parent_guid
    , p.code
    , p.description
    , p.hidden
    , p.placeholder
    , CASE
        WHEN sp.name::text = 'Root Account'::text THEN p.name
        ELSE (((sp.longname::text || '->'::text) || p.name::text))::character varying(2048)
      END AS longname
    , sp.depth + 1

FROM gnucash.accounts p
    JOIN raccounts sp
        ON p.parent_guid::text = sp.guid::text
    LEFT JOIN reporting.commodities c_1
        ON sp.commodity_guid::text = c_1.guid::text

),

accountrows AS (

SELECT
    raccounts.guid
    , raccounts.name
    , raccounts.account_type
    , raccounts.commodity_guid
    , raccounts.commodity_scu
    , raccounts.non_std_scu
    , raccounts.parent_guid
    , raccounts.code
    , raccounts.description
    , raccounts.hidden
    , raccounts.placeholder
    , raccounts.longname
    , raccounts.depth
    , row_number() OVER (ORDER BY raccounts.code NULLS FIRST) AS intid

FROM raccounts

)

SELECT
    ar.intid
    , ar.guid
    , ar.name
    , CASE
        WHEN ar.account_type::text IN ('ASSET', 'BANK', 'CASH', 'MUTUAL', 'STOCK') THEN 'ASSET'::character varying
        WHEN ar.account_type::text = 'CREDIT'::text THEN 'LIABILITY'::character varying
        ELSE ar.account_type
      END AS account_class
    , ar.account_type
    , ar.commodity_guid
    , c.namespace AS commodity_namespace
    , c.mnemonic AS commodity_mnemonic
    , ar.commodity_scu
    , ar.non_std_scu
    , ar.parent_guid
    , ar.code
    , ar.description
    , ar.hidden
    , ar.placeholder
    , ar.longname
    , ar.depth
    , ar.intid * 2 - ar.depth AS lft
    , ar.intid * 2 - ar.depth + (( SELECT count(*) * 2 FROM accountrows ar2 WHERE ar2.longname::text ~~ (ar.longname::text || '->%'::text))) + 1 AS rgt

FROM accountrows ar
	LEFT JOIN reporting.commodities c
		ON ar.commodity_guid::text = c.guid::text

ORDER BY
    ar.code

WITH DATA;
