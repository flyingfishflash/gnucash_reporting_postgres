-- View: reporting.balance_sheet

-- DROP VIEW reporting.balance_sheet;

CREATE OR REPLACE VIEW reporting.balance_sheet AS 
 
WITH cte_unrealizedgain AS (
SELECT
	'EQUITY'::character varying AS account_class
	, 3 AS sequence
	, NULL::integer AS intid
	, 'Unrealized'::character varying AS parentaccounts
	, 'Unrealized Gain'::character varying AS name
	, ( SELECT fnt_portfoliobasisvalue.value - fnt_portfoliobasisvalue.basis AS unrealizedgain
	    FROM reporting.fnt_portfoliobasisvalue(now()::date ) fnt_portfoliobasisvalue(date, basis, value)
	) AS balance

),

cte_retainedincome AS (
SELECT
	'EQUITY'::character varying AS account_class
	, 2 AS sequence
	, NULL::integer AS intid
	, 'Retained'::character varying AS parentaccounts
	, 'Retained Income'::character varying AS name
	, - (( SELECT sum(a.balance) AS sum
		   FROM reporting.accounts_balances a
		   WHERE a.account_type::text = ANY (ARRAY['INCOME'::character varying::text, 'EXPENSE'::character varying::text]))
	  ) AS balance

),
        
cte_assetsliabilitiesequity AS (
SELECT
	CASE
		WHEN ab.account_class::text = 'ASSET'::text THEN 'ASSETS'::character varying
		WHEN ab.account_class::text = 'LIABILITY'::text THEN 'LIABILITIES'::character varying
		WHEN ab.account_class::text = 'EQUITY'::text THEN 'EQUITY'::character varying
		ELSE ab.account_class
	END AS account_class
	, 1 AS sequence
	, ab.intid
	, (  SELECT a.name
		 FROM reporting.accounts a
		 WHERE a.guid::text = ab.parent_guid::text
	  ) AS parentaccounts
	, ab.name
	, CASE
		WHEN ab.account_class::text = ANY (ARRAY['EQUITY'::character varying::text, 'LIABILITY'::character varying::text])
		THEN - (( 	SELECT sum(a.value) AS sum
					FROM reporting.accounts_balances a
					WHERE a.lft >= ab.lft AND a.rgt <= ab.rgt))
		ELSE (  SELECT sum(a.value) AS sum
				FROM reporting.accounts_balances a
				WHERE a.lft >= ab.lft AND a.rgt <= ab.rgt)
	  END AS balance

FROM reporting.accounts_balances ab

WHERE
	ab.depth = 4
	AND (ab.account_class::text = ANY (ARRAY['ASSET'::character varying::text, 'LIABILITY'::character varying::text, 'EQUITY'::character varying::text]))
	AND ab.hidden = 0

),
        
cte_balance_sheet AS (
SELECT
	cte_assetsliabilitiesequity.account_class
	, cte_assetsliabilitiesequity.sequence
	, cte_assetsliabilitiesequity.intid
	, cte_assetsliabilitiesequity.parentaccounts
	, cte_assetsliabilitiesequity.name
	, cte_assetsliabilitiesequity.balance

FROM cte_assetsliabilitiesequity

UNION

SELECT
	cte_retainedincome.account_class
	, cte_retainedincome.sequence
	, cte_retainedincome.intid
	, cte_retainedincome.parentaccounts
	, cte_retainedincome.name
	, cte_retainedincome.balance

FROM cte_retainedincome

UNION

SELECT
	cte_unrealizedgain.account_class
	, cte_unrealizedgain.sequence
	, cte_unrealizedgain.intid
	, cte_unrealizedgain.parentaccounts
	, cte_unrealizedgain.name
	, cte_unrealizedgain.balance

FROM cte_unrealizedgain

)
        
SELECT
	cte_balance_sheet.account_class
	, cte_balance_sheet.sequence
	, cte_balance_sheet.intid
	, cte_balance_sheet.parentaccounts
	, cte_balance_sheet.name
	, cte_balance_sheet.balance

FROM cte_balance_sheet

WHERE
	cte_balance_sheet.balance <> 0::numeric

ORDER BY
	cte_balance_sheet.sequence
	,  cte_balance_sheet.intid;
