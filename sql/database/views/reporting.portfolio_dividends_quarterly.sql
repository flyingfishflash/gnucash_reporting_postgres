-- View: reporting.portfolio_dividends_quarterly

-- DROP VIEW reporting.portfolio_dividends_quarterly;

CREATE OR REPLACE VIEW reporting.portfolio_dividends_quarterly AS 

WITH dividend_accounts AS (

SELECT DISTINCT
	portfolio_transactions.a_intid
	, portfolio_transactions.account

FROM reporting.portfolio_transactions

WHERE
	portfolio_transactions.source_account_type::text = 'INCOME'::text

),

monthenddates_quarterly AS (

SELECT
	dateseries_monthend.monthenddate
	, date_part('year'::text, dateseries_monthend.monthenddate)::integer AS year
	, date_part('quarter'::text, dateseries_monthend.monthenddate)::integer AS quarter

FROM reporting.dateseries_monthend

WHERE
	date_part('month'::text, dateseries_monthend.monthenddate) = ANY (ARRAY[1::double precision, 4::double precision, 7::double precision, 10::double precision])

),

accounts_medq AS (

SELECT
	da.a_intid
	, da.account
	, medq.monthenddate
	, medq.year
	, medq.quarter

FROM dividend_accounts da

CROSS JOIN monthenddates_quarterly medq

),

quarterly_dividends AS (

SELECT
	portfolio_transactions.a_intid
	, portfolio_transactions.account
	, date_part('year'::text, portfolio_transactions.post_date) AS year
	, date_part('quarter'::text, portfolio_transactions.post_date) AS quarter
	, sum(portfolio_transactions.value) AS sum

FROM reporting.portfolio_transactions

WHERE
	portfolio_transactions.source_account_type::text = 'INCOME'::text

GROUP BY
	date_part('year'::text, portfolio_transactions.post_date)
	, date_part('quarter'::text
	, portfolio_transactions.post_date)
	, portfolio_transactions.a_intid
	, portfolio_transactions.account

)

SELECT
	aq.a_intid
	, aq.account
	, aq.year
	, aq.quarter
	, pt.sum

FROM accounts_medq aq
	LEFT JOIN quarterly_dividends pt
		ON pt.year = aq.year::double precision
		AND pt.quarter = aq.quarter::double precision
		AND pt.a_intid = aq.a_intid

WHERE
	pt.sum IS NOT NULL

ORDER BY
	aq.account
	, aq.a_intid
	, aq.year
	, aq.quarter;
