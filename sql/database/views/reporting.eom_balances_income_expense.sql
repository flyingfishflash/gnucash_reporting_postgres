-- View: reporting.eom_balances_income_expense

-- DROP VIEW reporting.eom_balances_income_expense;

CREATE OR REPLACE VIEW reporting.eom_balances_income_expense AS 

WITH cte_balances AS (
SELECT
	reporting.fns_lastdaymonth(splits.post_date::date) AS balancedate,
	accounts.guid,
	sum(COALESCE(splits.quantity, 0::numeric)) AS transactionsbalance

FROM reporting.accounts
	JOIN reporting.splits
		ON accounts.guid::text = splits.account_guid::text

WHERE
	accounts.account_type::text = ANY (ARRAY['INCOME'::character varying::text, 'EXPENSE'::character varying::text])

GROUP BY
	accounts.guid
	, reporting.fns_lastdaymonth(splits.post_date::date)
	
)

SELECT
	b.balancedate,
	date_part('year'::text, b.balancedate) AS yearbalancedate,
	date_part('month'::text, b.balancedate) AS monthbalancedate,
	a.intid,
	a.guid,
	a.account_class,
	a.account_type,
	a.name,
	a.longname,
	a.code,
	a.depth,
	b.transactionsbalance,
	a.commodity_namespace,
	a.commodity_mnemonic
FROM cte_balances b

JOIN reporting.accounts a
	ON b.guid::text = a.guid::text;
