-- View: reporting.income_detail_iradistributions

-- DROP VIEW reporting.income_detail_iradistributions;

CREATE OR REPLACE VIEW reporting.income_detail_iradistributions AS 

SELECT
	s.post_date::date AS date
	, sum(s.value) AS value

FROM reporting.accounts a
	JOIN reporting.splits s ON a.guid::text = s.account_guid::text

WHERE
	a.longname::text ~~ '%Income%IRA%Distribution%'::text

GROUP BY
	s.post_date

ORDER BY
	s.post_date;

