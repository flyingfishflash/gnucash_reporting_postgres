-- View: reporting.income_expense_retained_yearly

-- DROP VIEW reporting.income_expense_retained_yearly;

CREATE OR REPLACE VIEW reporting.income_expense_retained_yearly AS 

WITH cte_yearly AS (

SELECT
	income_expense_retained_monthly.year
	, sum(income_expense_retained_monthly.income) AS income
	, sum(income_expense_retained_monthly.expense) AS expense
	, sum(income_expense_retained_monthly.expenseaftertax) AS expenseaftertax
	, sum(income_expense_retained_monthly.expensehousing) AS expensehousing
	, sum(income_expense_retained_monthly.retained) AS retained

FROM reporting.income_expense_retained_monthly

GROUP BY
	income_expense_retained_monthly.year

)

SELECT
	cte_yearly.year
	, cte_yearly.income
	, cte_yearly.expense
	, cte_yearly.expenseaftertax
	, cte_yearly.expensehousing
	, cte_yearly.retained
	, COALESCE(cte_yearly.retained / cte_yearly.income, 0::numeric) AS retainedpercentage

FROM cte_yearly

ORDER BY cte_yearly.year;
