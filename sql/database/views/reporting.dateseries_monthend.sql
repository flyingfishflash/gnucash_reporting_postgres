-- View: reporting.dateseries_monthend

-- DROP VIEW reporting.dateseries_monthend;

CREATE OR REPLACE VIEW reporting.dateseries_monthend AS 

SELECT
	(monthenddate1.monthenddate1 - '1 day'::interval)::date AS monthenddate

FROM generate_series(date_trunc('month'::text, reporting.fns_mintrandate()::timestamp with time zone) + '1 mon'::interval, date_trunc('month'::text, now()::date::timestamp with time zone) + '1 mon'::interval, '1 mon'::interval) monthenddate1(monthenddate1);
