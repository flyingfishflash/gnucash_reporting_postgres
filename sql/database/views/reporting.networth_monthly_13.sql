-- View: reporting.networth_monthly_13

-- DROP VIEW reporting.networth_monthly_13;

CREATE OR REPLACE VIEW reporting.networth_monthly_13 AS 

WITH cte_row_number AS (

SELECT
	row_number() OVER (ORDER BY networth_monthly.date) AS row_number
	, networth_monthly.date
	, networth_monthly.assets
	, networth_monthly.fluctuation_assets
	, networth_monthly.liabilities
	, networth_monthly.fluctuation_liabilities
	, networth_monthly.networth
	, networth_monthly.fluctuation

FROM reporting.networth_monthly

WHERE
	networth_monthly.date > (now() - '1 year'::interval)

)

SELECT
	cte_row_number.date
	, cte_row_number.assets
	, CASE
		WHEN cte_row_number.row_number = 1 THEN 0::numeric
		ELSE cte_row_number.fluctuation_assets
	  END AS fluctuation_assets
	, cte_row_number.liabilities
	, CASE
		WHEN cte_row_number.row_number = 1 THEN 0::numeric
		ELSE cte_row_number.fluctuation_liabilities
      END AS fluctuation_liabilities
	, cte_row_number.networth
	, CASE
		WHEN cte_row_number.row_number = 1 THEN 0::numeric
		ELSE cte_row_number.fluctuation
	  END AS fluctuation

FROM cte_row_number;
