-- View: reporting.income_expense_detail_yearly

-- DROP VIEW reporting.income_expense_detail_yearly;

CREATE OR REPLACE VIEW reporting.income_expense_detail_yearly AS 

WITH cte_splits AS (
SELECT
	date_trunc('year'::text, s.post_date::date::timestamp with time zone) + '1 year'::interval - '1 day'::interval AS post_monthenddate
	, a_1.account_class
	, CASE
		WHEN a_1.depth > 4
		THEN ( SELECT a1.guid
			   FROM reporting.accounts a1
			   WHERE a1.lft < a_1.lft AND a1.depth = 4
			   ORDER BY a1.lft DESC
			   LIMIT 1)
		ELSE a_1.guid
	  END AS detail_level_a_guid
    , s.quantity

FROM reporting.splits s

INNER JOIN reporting.accounts a_1 
	ON a_1.guid::text = s.account_guid::text

WHERE
	(a_1.account_class::text = 'INCOME'::text OR a_1.account_class::text = 'EXPENSE'::text)

),

cte_splits_sum_by_detail_level AS (
SELECT
	cte_splits.post_monthenddate
	, cte_splits.account_class
	, cte_splits.detail_level_a_guid
	, sum(cte_splits.quantity) AS balance

FROM cte_splits

GROUP BY
	cte_splits.post_monthenddate
	, cte_splits.account_class
	, cte_splits.detail_level_a_guid

)

SELECT
	me.yearenddate AS monthenddate
	, detail_level_sums.account_class
	, detail_level_a.intid AS detail_level_a_intid
	, detail_level_a.name
    , detail_level_a.depth
	, detail_level_sums.balance
	, detail_level_sums.detail_level_a_guid
    , detail_level_less1_a.guid as detail_level_less1_a_guid
    , detail_level_less2_a.guid as detail_level_less2_a_guid

FROM reporting.dateseries_yearend me
	LEFT JOIN cte_splits_sum_by_detail_level detail_level_sums
		ON detail_level_sums.post_monthenddate = me.yearenddate
	LEFT JOIN reporting.accounts detail_level_a
		ON detail_level_a.guid::text = detail_level_sums.detail_level_a_guid::text
    LEFT JOIN reporting.accounts detail_level_less1_a
    	ON detail_level_less1_a.guid = detail_level_a.parent_guid
    LEFT JOIN reporting.accounts detail_level_less2_a
    	ON detail_level_less2_a.guid = detail_level_less1_a.parent_guid

ORDER BY
	me.yearenddate, detail_level_a.lft;

