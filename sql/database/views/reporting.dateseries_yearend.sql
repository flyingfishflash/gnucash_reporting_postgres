-- View: reporting.dateseries_yearend

-- DROP VIEW reporting.dateseries_yearend;

CREATE OR REPLACE VIEW reporting.dateseries_yearend AS 

SELECT
	(yearenddate1.yearenddate1 - '1 day'::interval)::date AS yearenddate

FROM generate_series(date_trunc('year'::text, reporting.fns_mintrandate()::timestamp with time zone) + '1 year'::interval, date_trunc('year'::text, now()) + '1 year'::interval, '1 year'::interval) yearenddate1(yearenddate1);
