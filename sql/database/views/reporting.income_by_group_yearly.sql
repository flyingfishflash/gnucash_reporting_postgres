-- View: reporting.income_by_group_yearly

-- DROP VIEW reporting.income_by_group_yearly;

CREATE OR REPLACE VIEW reporting.income_by_group_yearly AS

SELECT
	y.monthenddate
	, a.name
	, a.lft
	, sum(y.balance) AS sum

FROM reporting.income_expense_detail_yearly y
	LEFT JOIN reporting.accounts a ON a.guid::text = y.detail_level_less1_a_guid::text

WHERE
	y.account_class::text = 'INCOME'::text

GROUP BY
	y.monthenddate
	, a.name
	, a.lft

ORDER BY
	y.monthenddate
	, a.lft;

