-- View: reporting.eom_balances_assets_liabilities

-- DROP VIEW reporting.eom_balances_assets_liabilities;

CREATE OR REPLACE VIEW reporting.eom_balances_assets_liabilities AS 

WITH cte_accounts AS (

SELECT
	a.account_class
	, a.commodity_guid AS c_guid
	, a.guid AS a_guid

FROM reporting.accounts a

WHERE
	a.account_class::text = 'ASSET'::text
	OR a.account_class::text = 'LIABILITY'::text

),

cte_monthenddates_accounts AS (

SELECT
	dateseries_monthend.monthenddate
	, ca.account_class
	, ca.c_guid
	, ca.a_guid

FROM reporting.dateseries_monthend

CROSS JOIN cte_accounts ca

),

cte_splits_sum AS (

SELECT
	reporting.fns_lastdaymonth(s.post_date::date) AS post_lastdaymonth
	, a.account_class
	, a.c_guid
	, a.a_guid
	, sum(s.quantity) AS transactionbalance

FROM reporting.splits s

JOIN cte_accounts a
	ON s.account_guid::text = a.a_guid::text

WHERE
	a.account_class::text = ANY (ARRAY['ASSET'::character varying::text, 'LIABILITY'::character varying::text])

GROUP BY
	reporting.fns_lastdaymonth(s.post_date::date)
	, a.account_class
	, a.c_guid, a.a_guid

),

cte_monthend_transactionbalance AS (

SELECT
	ma.monthenddate
	, ma.account_class
	, ma.c_guid
	, ma.a_guid
	, COALESCE(css.transactionbalance, 0::numeric) AS accountbalance

FROM cte_monthenddates_accounts ma

LEFT JOIN cte_splits_sum css
	ON ma.monthenddate = css.post_lastdaymonth
	AND ma.a_guid::text = css.a_guid::text

),

cte_monthend_transactionvalue AS (

SELECT
	cmtb.monthenddate
	, cmtb.account_class
	, cmtb.c_guid
	, cmtb.a_guid
	, cmtb.accountbalance
	, CASE
		WHEN cmtb.accountbalance <> 0::numeric THEN COALESCE(( SELECT (reporting.fnt_nearestcommodityprice(cmtb.c_guid, cmtb.monthenddate, (-3))).price AS price), 1::numeric) * cmtb.accountbalance
		ELSE 0::numeric
	  END AS accountvalue

FROM cte_monthend_transactionbalance cmtb

),

cte_monthend_accountbalances AS (

SELECT
	cmtv.monthenddate AS balancedate
	, cmtv.account_class
	, cmtv.a_guid
	, cmtv.c_guid
	, sum(cmtv.accountbalance) OVER (PARTITION BY cmtv.a_guid ORDER BY cmtv.monthenddate, cmtv.a_guid) AS balance
	, sum(cmtv.accountvalue) OVER (PARTITION BY cmtv.a_guid ORDER BY cmtv.monthenddate, cmtv.a_guid) AS value
	, a.intid
	, a.account_type
	, a.name
	, a.longname
	, a.code
	, a.depth
	, a.commodity_namespace
	, a.commodity_mnemonic

FROM cte_monthend_transactionvalue cmtv
	JOIN reporting.accounts a
		ON cmtv.a_guid::text = a.guid::text

)

SELECT
	cmab.balancedate
	, cmab.account_class
	, cmab.a_guid
	, cmab.c_guid
	, cmab.balance
	, cmab.value
	, cmab.intid
	, cmab.account_type
	, cmab.name
	, cmab.longname
	, cmab.code
	, cmab.depth
	, cmab.commodity_namespace
	, cmab.commodity_mnemonic

FROM cte_monthend_accountbalances cmab

ORDER BY
	cmab.balancedate
	, cmab.account_class
	, cmab.intid;
