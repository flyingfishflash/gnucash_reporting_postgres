-- View: reporting.accounts_balances_assetsliabilities

-- DROP VIEW reporting.accounts_balances_assetsliabilities;

CREATE OR REPLACE VIEW reporting.accounts_balances_assetsliabilities AS 

SELECT
	ab.intid
	, ab.account_class
	, ab.account_type
	, CASE
	    WHEN ab.depth > 4
	    THEN COALESCE(
            (SELECT a.name
             FROM reporting.accounts a
             WHERE a.lft < ab.lft AND a.rgt > ab.rgt AND a.depth = 4
             LIMIT 1),
            (SELECT a.name
             FROM reporting.accounts_balances a
             WHERE a.lft < ab.lft AND a.rgt > ab.rgt AND a.depth <= 4
             ORDER BY a.lft DESC
             LIMIT 1),
            'nogroup'::character varying)
        ELSE ab.name
      END AS accountgroup
	, CASE
        WHEN ab.depth > 4
        THEN COALESCE(
           (SELECT a.intid
            FROM reporting.accounts a
            WHERE a.lft < ab.lft AND a.rgt > ab.rgt AND a.depth = 4
            LIMIT 1),
           (SELECT a.intid
            FROM reporting.accounts_balances a
            WHERE a.lft < ab.lft AND a.rgt > ab.rgt AND a.depth <= 4
            ORDER BY a.lft DESC
            LIMIT 1))
        ELSE ab.intid
	  END AS accountgroupintid
	, ab.code
	, ab.name
	, ab.longname
	, ab.balance
	, ab.value
	, ab.pricedate
	
	FROM reporting.accounts_balances ab

	WHERE
		ab.value <> 0::numeric 
		AND ab.account_class in ('ASSET', 'LIABILITY');
