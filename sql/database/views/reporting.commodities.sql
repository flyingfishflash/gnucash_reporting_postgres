-- Materialized View: reporting.commodities

-- DROP MATERIALIZED VIEW reporting.commodities;

CREATE MATERIALIZED VIEW reporting.commodities AS 

SELECT
	commodities.guid
	, commodities.namespace
	, commodities.mnemonic
	, commodities.fullname
	, commodities.cusip
	, commodities.fraction
	, commodities.quote_flag
	, commodities.quote_source
	, commodities.quote_tz

FROM gnucash.commodities
WITH DATA;
