create unique index on reporting.accounts(guid);
create unique index on reporting.accounts(intid);
create index accounts_intid_idx_account_class on reporting.accounts (intid) where depth =4 and hidden =0 and account_class in ('ASSET', 'LIABILITY', 'EQUITY');
create index on reporting.accounts(account_class);
create index on reporting.accounts(account_type);
create index on reporting.accounts(name);
create index on reporting.accounts(depth);
create index on reporting.accounts(parent_guid);
create index on reporting.accounts(lft);

create unique index on reporting.commodities(guid);
create index on reporting.commodities (guid, mnemonic);

create unique index on reporting.prices (commodity_guid asc nulls last, date asc nulls last, price asc nulls last);

create unique index on reporting.splits (guid);
create index on reporting.splits (account_guid asc, tx_guid asc, value asc, quantity asc);

create unique index on reporting.transactions(guid);
