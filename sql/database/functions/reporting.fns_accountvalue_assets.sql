-- Function: reporting.fns_accountvalue_assets(character varying, date, date, boolean)

-- DROP FUNCTION reporting.fns_accountvalue_assets(character varying, date, date, boolean);

CREATE OR REPLACE FUNCTION reporting.fns_accountvalue_assets(
    accountguid character varying,
    startdate date DEFAULT '1900-01-01'::date,
    enddate date DEFAULT (now())::date,
    descend boolean DEFAULT false)
RETURNS numeric AS

$BODY$

declare
	accountvalue numeric(18,8) := 0;

begin

	--raise info 'accountguid %, startdate %, enddate %, descend %', accountguid, startdate, enddate, descend;

	if descend::boolean = 'f'
	then

		select sum(valueperaccount.accountvalue)

		into accountvalue

		from
			(select
				a.guid 
			,	a.commodity_guid
			,	coalesce(reporting.fns_sum_splits(accountguid:=a.guid, startdate:=startdate::date, enddate:=enddate::date, descend:='f'::boolean), 0)
				* coalesce((select price from reporting.fnt_nearestcommodityprice(a.commodity_guid, enddate::date, -3)), 1) as accountvalue

			from reporting.accounts a

			where
				account_class = upper('asset')
				and a.guid = accountguid

			group by
				a.guid, a.commodity_guid) valueperaccount;

		return accountvalue;

	else

		select sum(valueperaccount.accountvalue)

		into accountvalue

		from
			(select
				da.guid 
			,	da.commodity_guid
			,	coalesce(reporting.fns_sum_splits(accountguid:=da.guid, startdate:=startdate::date, enddate:=enddate::date, descend:='f'::boolean), 0)
				* coalesce((select price from reporting.fnt_nearestcommodityprice(da.commodity_guid, enddate::date, -3)), 1) as accountvalue

			from reporting.accounts a
				inner join reporting.accounts da
					on a.guid = accountguid
					and da.lft between a.lft and a.rgt
				
			group by
				da.guid
			,	da.commodity_guid
			) valueperaccount;

		return accountvalue;

	end if;

end;

$BODY$
  LANGUAGE plpgsql STABLE
  COST 100;
