-- Function: reporting.fnt_rpt_portfolio(date)

-- DROP FUNCTION reporting.fnt_rpt_portfolio(date);

CREATE OR REPLACE FUNCTION reporting.fnt_rpt_portfolio(IN enddate date DEFAULT (now())::date)
RETURNS TABLE(transaction_guid character varying, commodity_guid character varying, account_intid integer, account_guid character varying, account character varying, lotid integer, open_date date, open_price numeric, close_date date, close_price numeric, quantity numeric, lot_balance numeric, basis numeric, value numeric, funds_in numeric, funds_out numeric, income numeric, brokerage_fees numeric, realized_gain numeric, unrealized_gain numeric, total_gain numeric, source_account_type character varying) AS

$BODY$

declare transaction record;


loopbalance numeric(18,8) := 0;
wlotid integer := 0;
currentlotbalance numeric(18,8) := 0;
currentlotprice numeric(18,8) := 0;
currentrealizedgain numeric(18,8) := 0;
newlotbalance numeric(18,8) := 0;
currentfundsout numeric(18,8) := 0;
newrealizedgain numeric(18,8) := 0;
newfundsout numeric(18,8) := 0;


begin


	drop table if exists lots;
	create temp table lots (
	  lotid serial unique
	, account_intid int
	, account_guid varchar(32)
	, commodity_guid varchar(2048)
	, transaction_guid varchar(2048)
	, split_guid varchar(2048)
	, account varchar(2048)
	, open_date date
	, open_price numeric(18,8) default 0
	, close_date date
	, close_price numeric(18,8) default 0
	, quantity numeric(18,8) default 0
	, lot_balance numeric(18,8) default 0
	, basis numeric(18,8) default 0
	, value numeric(18,8) default 0
	, funds_in numeric(18,8) default 0
	, funds_out numeric(18,8) default 0
	, income numeric(18,8) default 0
	, brokerage_fees NUMeric(18,8) default 0
	, realized_gain numeric(18,8) default 0
	, unrealized_gain numeric(18,8) default 0
	, total_gain numeric(18,8) default 0
	, total_return numeric(18,8) default 0
	, source_account_type varchar(2048)
	);

	for transaction in (select * from reporting.portfolio_transactions where post_date::date <= enddate::date) loop

		raise notice '%, %, %',transaction.account, transaction.quantity, transaction.source_account_type;


		loopbalance := transaction.quantity;

		if transaction.quantity >= 0
		then

			insert into lots (account_intid, account_guid, commodity_guid, transaction_guid, split_guid, account, open_date/*post_date*/, open_price/*price*/, quantity, lot_balance, funds_in, funds_out, income, brokerage_fees, source_account_type)
			values (
			  transaction.a_intid 
			, transaction.a_guid
			, transaction.c_guid
			, transaction.t_guid
			, transaction.s_guid
			, transaction.account
			, transaction.post_date --open_date
			, transaction.price
			, transaction.quantity
			, transaction.quantity --lot_balance
			, case when transaction.source_account_type = 'INCOME' then 0 else transaction.fundsin end -- only counts funds in from non-income (theoretically re-invested dividend) transactions
			, transaction.fundsout
			, transaction.income
			, coalesce((select sum(s.value) from reporting.splits s inner join reporting.accounts a on s.account_guid = a.guid and a.account_type = 'EXPENSE' where transaction.t_guid = s.tx_guid and s.post_date::date <= enddate::date), 0)
			, transaction.source_account_type
			);

-- 			update lots
-- 			set lot_balance = currentlotbalance + transaction.quantity
-- 			where lots.lotid = wlotid;

		else

			-- update brokerage fees for a sell transaction
			update lots set brokerage_fees = coalesce((select sum(s.value) from reporting.splits s inner join reporting.accounts a on s.account_guid = a.guid and a.account_type = 'EXPENSE' where transaction.t_guid = s.tx_guid and s.post_date::date <= enddate::date), 0);

			while loopbalance < 0 loop

				wlotid := (select min(lots.lotid) from lots where lots.lot_balance > 0 and lots.account_intid = transaction.a_intid);
				currentlotbalance := (select lots.lot_balance from lots where lots.lotid = wlotid);
				currentlotprice := (select lots.open_price from lots where lots.lotid = wlotid);
				currentrealizedgain := coalesce((select lots.realized_gain from lots where lots.lotid = wlotid), 0);
				newlotbalance := (select lots.lot_balance + loopbalance from lots where lots.lotid = wlotid);
				currentfundsout := (select lots.funds_out from lots where lots.lotid = wlotid);
				newlotbalance := (select lots.lot_balance + loopbalance from lots where lots.lotid = wlotid);

				if (select lots.account_intid from lots where lots.split_guid = transaction.s_guid) = 47
				then
					raise info '%, %, %, %, %, %', transaction.a_intid, transaction.post_date, transaction.quantity, currentlotprice, newlotbalance, loopbalance;
				end if; 

				if newlotbalance > 0
				then

					newrealizedgain := currentrealizedgain - (loopbalance * (transaction.price - currentlotprice));
					newfundsout := currentfundsout + (transaction.price * loopbalance);

					update lots set
						lot_balance = newlotbalance
						, realized_gain = newrealizedgain
						, funds_out = newfundsout
					where lots.lotid = wlotid;

					loopbalance := 0;

				else

					newrealizedgain := currentrealizedgain + (currentlotbalance * (transaction.price - currentlotprice));
					newfundsout := currentfundsout - (currentlotbalance * transaction.price);

					update lots set
					  lot_balance = 0
					, realized_gain = newrealizedgain
					, funds_out = newfundsout
					, close_date = transaction.post_date
					, close_price = transaction.price
					where lots.lotid = wlotid;

					loopbalance = newlotbalance;
					

				end if; --newlotbalance > 0
				
			end loop; -- loopbalance < 0

		end if; -- transaction.quantity >= 0

		
		
	end loop;

	--raise info "calculated basis : %'", lots.lot_balance
	update lots set basis = lots.lot_balance * lots.open_price;
	--update lots set basis = lots.funds_in + lots.income + lots.funds_out;
	update lots set value = lots.lot_balance * (select price from reporting.fnt_nearestcommodityprice(lots.commodity_guid, enddate::date, -2));
	update lots set funds_in = lots.funds_in + lots.brokerage_fees;
	update lots set unrealized_gain = lots.value - (lots.lot_balance * lots.open_price);
	update lots set realized_gain = lots.realized_gain - lots.brokerage_fees;
	update lots set total_gain = lots.realized_gain + lots.unrealized_gain;

	return query
	select
	  l.transaction_guid
	, l.commodity_guid
	, l.account_intid
	, l.account_guid
	, l.account
	, l.lotid
	, l.open_date
	, l.open_price
	, l.close_date
	, l.close_price
	, l.quantity
	, l.lot_balance
	, l.basis
	, l.value
	, l.funds_in
	, l.funds_out
	, l.income
	, l.brokerage_fees
	, l.realized_gain
	, l.unrealized_gain
	, l.total_gain
	, l.source_account_type
	from lots l;


end;


$BODY$
LANGUAGE plpgsql VOLATILE
COST 100
ROWS 1000;
