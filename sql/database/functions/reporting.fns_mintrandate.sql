-- Function: reporting.fns_mintrandate()

-- DROP FUNCTION reporting.fns_mintrandate();

CREATE OR REPLACE FUNCTION reporting.fns_mintrandate()
RETURNS date AS

$BODY$

  select min(post_date)::date from reporting.transactions;

$BODY$

LANGUAGE sql STABLE
COST 100;

