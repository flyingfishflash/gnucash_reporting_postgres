-- Function: reporting.fnt_midmonthandmonthenddates(date, date)

-- DROP FUNCTION reporting.fnt_midmonthandmonthenddates(date, date);

CREATE OR REPLACE FUNCTION reporting.fnt_midmonthandmonthenddates(
    date1 date DEFAULT (now())::date,
    date2 date DEFAULT (now())::date)
RETURNS SETOF record AS

$BODY$

declare
monthenddate record;
startdate date;
enddate date;
date1 date := reporting.fns_lastdaymonth(date1);
date2 date := reporting.fns_lastdaymonth(date2);


begin

if date1 <= date2 then
	startdate:=date1; enddate:=date2;
else
	startdate:=date2; enddate:=date1;
end if;

raise info 'fnt_monthenddates: % -> %',startdate, enddate;

if startdate > enddate then
	raise exception 'The start date cannot be greater than the end date';
else
	while startdate <= enddate loop

		select startdate into monthenddate;
		return next monthenddate;

		if date_part('day', startdate) = 15 then
			--startdate:=(startdate + interval '1 month')::date;
			startdate:=(reporting.fns_lastdaymonth(startdate));
		else
			startdate:=(startdate + interval '15 day')::date;
		end if;
			
	end loop;
end if;

end;

$BODY$

LANGUAGE plpgsql STABLE
COST 100
ROWS 1000;
