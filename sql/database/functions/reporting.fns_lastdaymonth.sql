-- Function: reporting.fns_lastdaymonth(date)

-- DROP FUNCTION reporting.fns_lastdaymonth(date);

CREATE OR REPLACE FUNCTION reporting.fns_lastdaymonth(date)
RETURNS date AS

$BODY$

  SELECT (date_trunc('MONTH', $1) + INTERVAL '1 MONTH - 1 day')::date;

$BODY$

  LANGUAGE sql IMMUTABLE STRICT
  COST 100;

