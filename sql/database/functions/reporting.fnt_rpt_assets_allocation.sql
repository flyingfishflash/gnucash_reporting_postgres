﻿-- Function: reporting.fnt_rpt_assets_allocation()

-- DROP FUNCTION reporting.fnt_rpt_assets_allocation();

CREATE OR REPLACE FUNCTION reporting.fnt_rpt_assets_allocation()
RETURNS TABLE(intid bigint, assetid integer, account_type character varying, name character varying, balance numeric, monthlyexpenses numeric, unplannedexpenses numeric, lostincomereserve numeric, vacation numeric, house numeric, wildcard numeric, longterminvestment numeric, notallocated numeric) AS

$BODY$

declare
-- Variables
startdate date := (select date_trunc('month', (min(post_date))) from reporting.transactions); --first day of the month of the oldest transaction date
enddate date:=(select reporting.fns_lastdaymonth (max(post_date)::date) from reporting.transactions); --last day of the month of the newest transaction date
sqlstatement varchar(8192);

-- Variables for the AllocationBuckets
loop_bucketid integer := 1;
bucket character varying(255);
firstaccount integer;
lastaccount integer;
bucketlimit numeric := 0;

-- Variables for AssetBalances
rollingbucketvolume numeric := 0;
notallocated numeric := 0;
accountbalance numeric := 0;
loop_assetid integer := 1;
deduction numeric := 0;


begin

	drop table if exists assetbalances;
	create temp table assetbalances (
		  intid bigint
		, assetid integer
		, account_type character varying(2048)
		, name character varying(2048)
		, balance numeric
		, monthlyexpenses numeric
		, unplannedexpenses numeric
		, lostincomereserve numeric
		, vacation numeric
		, house numeric
		, wildcard numeric
		, longterminvestment numeric
		, notallocated numeric
	);

	drop table if exists allocationbuckets;
	create temp table allocationbuckets (
		  id serial unique
		, bucket character varying(255)
		, firstaccount integer
		, lastaccount integer
		, bucketlimit numeric
	);

	insert into assetbalances (intid, assetid, account_type, name, balance, monthlyexpenses, unplannedexpenses, lostincomereserve, vacation, wildcard, house, longterminvestment, notallocated)
	select 
		  accounts.intid
		  -- This sets the order of the accounts which is assumed to exist further on
		, case 
			when accounts.name like 'Cash' then 1
			when accounts.name like 'Bonds' then 2
			when accounts.name like 'IRA' then 3
			when accounts.name like '401k%' then 4
			when accounts.name like 'Cash Balance Plans' then 5
			when accounts.name like 'Health Savings Accounts' then 6
			when accounts.name like 'Investments' then 7
		    else 0 
		end
		, accounts.account_type
		, accounts.name
		, reporting.fns_accountvalue_assets(accountguid:=accounts.guid, enddate:=enddate::date, descend:='t'::boolean)
		, 0
		, 0
		, 0
		, 0
		, 0
		, 0
		, 0
		, reporting.fns_accountvalue_assets(accountguid:=accounts.guid, enddate:=enddate::date, descend:='t'::boolean)

	from reporting.accounts

	where
		accounts.account_type in ('ASSET', 'BANK', 'CASH', 'STOCK', 'TRADING', 'MUTUAL')
		and accounts.hidden = 0
		and accounts.name not like 'Imbalance%'
		and accounts.longname not like '%Fixed Assets%'
		and accounts.depth = 4

	order by
		accounts.intid;

	insert into allocationbuckets(bucket, firstaccount, lastaccount, bucketlimit) values
	  ('monthlyexpenses', 1, 1, 2500)
	, ('unplannedexpenses', 1, 1, 2500)
	, ('lostincomereserve', 1, 1, 54000)
	, ('vacation', 1, 1, 4000)
	, ('house', 1, 1, 20000)
	, ('wildcard', 1, 1, 8000)
	, ('longterminvestment', 2, 7, 99999999);

	-- Loop through all the buckets, filling them based on the rules defined in @AllocationBuckets
	while exists (select ID from allocationbuckets where ID = loop_bucketid)
	loop
		bucket := (select allocationbuckets.bucket from allocationbuckets where id = loop_bucketid);
		firstaccount := (select allocationbuckets.firstaccount from allocationbuckets where id = loop_bucketid);
		lastaccount := (select allocationbuckets.lastaccount from allocationbuckets where id = loop_bucketid);
		bucketlimit := (select allocationbuckets.bucketlimit from allocationbuckets where id = loop_bucketid);
		loop_assetid := firstaccount;

		-- Loop through the accounts from FirstAccount to LastAccount deducting funds
		-- from the NotAllocated balance until either it's exhausted or the amount
		-- deducted is equal to the BucketLimit
		while (loop_assetid <= lastaccount) and (rollingbucketvolume < bucketlimit)
		loop
			notallocated := (select assetbalances.notallocated from assetbalances where assetbalances.assetid = loop_assetid);

			if notallocated > bucketlimit
			then
				deduction := (bucketlimit - rollingbucketvolume);
				rollingbucketvolume := bucketlimit;
				notallocated := notallocated - deduction;

			else
				deduction := notallocated;
				rollingbucketvolume := rollingbucketvolume + notallocated;
				notallocated := 0;
			end if;
			
			sqlstatement = 'update assetbalances ' ||
                           'set ' ||
                           'notallocated = ' || cast(notallocated as character varying) ||
                           ', "' || bucket || '" = ' || cast(deduction as character varying) || ' ' ||
                           'where assetbalances.assetid = ' || cast(loop_assetid as character varying);
			
			raise info '%', sqlstatement;

			execute sqlstatement;

			loop_assetid = loop_assetid + 1;

		end loop;

		raise info '%, %, %, %', bucket, firstaccount, lastaccount, bucketlimit;
		rollingbucketvolume = 0;
		loop_bucketid := loop_bucketid + 1;

	end loop;
	



	return query
	select
	*
	from assetbalances
	order by
		assetid;

end;

$BODY$

LANGUAGE plpgsql VOLATILE
COST 100
ROWS 1000;
