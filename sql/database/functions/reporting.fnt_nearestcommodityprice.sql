-- Function: reporting.fnt_nearestcommodityprice(character varying, date, integer)

-- DROP FUNCTION reporting.fnt_nearestcommodityprice(character varying, date, integer);

CREATE OR REPLACE FUNCTION reporting.fnt_nearestcommodityprice(
    IN commodityguid character varying,
    IN targetdate date DEFAULT (now())::date,
    IN maxdateoffset integer DEFAULT 3)
RETURNS TABLE(c_guid character varying, targeteddate date, targeteddateoffset integer, pricedate date, price numeric, offsetfromtarget integer) AS

$BODY$

	WITH cte_nearestprice
	AS
	(
		select
			c.guid
			, p.date as nearestpricedate
			, p.price as nearestprice
			, case when p.date::date is not null then 
				(p.date::date - ($2::date))
			end
			as dateoffset

		from reporting.prices p
			inner join reporting.commodities c
				on c.guid = p.commodity_guid

		where
			c.guid = commodityguid
			and p.date::date <= $2::date

		order by
			p.date desc

		limit 1
	)

	select
		  $1 --guid
		, $2 --targetdate
		, $3 --maximum date difference from target in days
		, coalesce(np.nearestpricedate, null)::date
		, coalesce(np.nearestprice, 0)
		, np.dateoffset
	from cte_nearestprice np

$BODY$

LANGUAGE sql VOLATILE
COST 100
ROWS 1;
