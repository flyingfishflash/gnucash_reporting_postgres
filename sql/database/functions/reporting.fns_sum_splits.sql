-- Function: reporting.fns_sum_splits(character varying, date, date, boolean)

-- DROP FUNCTION reporting.fns_sum_splits(character varying, date, date, boolean);

CREATE OR REPLACE FUNCTION reporting.fns_sum_splits(
    accountguid character varying,
    startdate date DEFAULT '1900-01-01'::date,
    enddate date DEFAULT (now())::date,
    descend boolean DEFAULT false)
RETURNS numeric AS

$BODY$

declare
	accountbalance numeric(18,8) := 0;

begin

/*
 	if startdate = '1900-01-01'::date
	then
 		startdate:=(select min(post_date) from reporting.splits);
 	end if;
*/

	raise info 'fns_sum_splits: accountguid %, startdate %, enddate %, descend %', accountguid, startdate, enddate, descend;

	if descend::boolean = 'f'
	then
		select
			coalesce(sum(coalesce(s.quantity, 0)), 0)

		into accountbalance
		
		from reporting.accounts a
			inner join reporting.splits s
				on a.guid = s.account_guid
		where 
			(a.guid = accountguid)
			and ((post_date::date >= startdate and post_date::date <= enddate)
			or post_date is null);

		return accountbalance;

	else
		with accounts_list
		as (
				select 
					da.intid
				,	da.guid
				,	da.name
				,	da.longname
				
				from reporting.accounts a
					inner join reporting.accounts da
						on a.guid = accountguid
						and da.lft between a.lft and a.rgt
		)
		

		select
			coalesce(sum(coalesce(s.quantity, 0)), 0)

		into accountbalance
		
		from accounts_list al
			inner join reporting.splits s
				on al.guid = s.account_guid
		where 
			((post_date::date >= startdate and post_date::date <= enddate)
			or post_date is null);

		return accountbalance;

/*
		REMOVED IN FAVOR OF THE ABOVE CODE 2013-05-13
		THE BELOW CODE CAUSED INACCURACIES WHEN SUMMING SPLITS FROM NESTED NON-PLACEHOLDER ACCOUNTS

 		select
 			coalesce(sum(coalesce(s.quantity, 0)), 0)
 
 		into accountbalance
 		
 		from reporting.accounts a
 			inner join reporting.accounts da
 				on a.guid = accountguid
 				and da.lft between a.lft and a.rgt
 			inner join reporting.splits s
 				on a.guid = s.account_guid or da.guid = s.account_guid
 		where 
 			((post_date::date >= startdate and post_date::date <= enddate)
 			or post_date is null);
 
 		return accountbalance;
*/

	end if;
	
end;
	

$BODY$

LANGUAGE plpgsql STABLE
COST 100;