-- Function: dba.fn_refresh_reporting()

-- Creating this function with the SECURITY DEFINER parameter allows
-- it to be executed with the permissions of it's creator.
-- This enables a readonly user to refresh the materialized views.

-- DROP FUNCTION dba.fn_refresh_reporting();

CREATE OR REPLACE FUNCTION dba.fn_refresh_reporting()
RETURNS void
SECURITY DEFINER
AS

$BODY$

refresh materialized view reporting.commodities with data;
refresh materialized view reporting.accounts with data;
refresh materialized view reporting.prices with data;
refresh materialized view reporting.splits with data;
refresh materialized view reporting.transactions with data;

$BODY$

LANGUAGE sql VOLATILE
COST 100;
