-- Function: reporting.fnt_rpt_portfolio_summary(date)

-- DROP FUNCTION reporting.fnt_rpt_portfolio_summary(date);

CREATE OR REPLACE FUNCTION reporting.fnt_rpt_portfolio_summary(IN enddate date DEFAULT (now())::date)
RETURNS TABLE(account_intid integer, commodity_guid character varying, price numeric, pricedate date, account character varying, lot_balance numeric, basis numeric, value numeric, funds_in numeric, funds_out numeric, realized_gain numeric, unrealized_gain numeric, total_gain numeric, rate_of_gain numeric, income numeric, brokerage_fees numeric, total_return numeric, rate_of_return numeric) AS

$BODY$

/*

Basis calculation method: FIFO
Basis is calculated using transaction pricing, not the price table.
Value: Share balance * market price as of the price date. Market Price is always from the price table.
Funds In: Funds entering the account including Brokerage Fees, excluding reinvested dividends.
Funds Out: Funds exiting the account due to share sale.
Realized Gain: Funds received from share sales minus the basis for shares sold, plus any brokerage fees.
Unrealized Gain: Values less the basis for unsold shares.
Total Gain: Realized Gain + Unrealized Gain
Rate of Gain: Total Gain / Money In * 100
Income: Value of all income transactions associated with an account.
Total Return: Total Gain + Income
Rate of Return: Total Return / Money In * 100

Brokerage fees are identified as any split crediting an expense account that's also part of a transaction with splits that hit stock, mutual, or fund accounts.

*/

with cte_portfoliosummary
as (

	select
	  p.account_intid
	, p.commodity_guid
	, p.account
	, sum(p.lot_balance) as lot_balance
	, sum(p.basis) as basis
	, sum(p.value) as value
	, sum(p.funds_in) as funds_in
	, sum(p.funds_out) as funds_out
	, sum(p.income) as income
	, sum(p.brokerage_fees) as brokerage_fees
	, sum(p.realized_gain) as realized_gain
	, sum(p.unrealized_gain) as unrealized_gain
	, sum(p.total_gain) as total_gain
	, sum(p.total_gain) + sum(p.income) as total_return

	from reporting.fnt_rpt_portfolio(enddate) p

	group by
	  p.account_intid
	, p.account
	, p.commodity_guid

)

select
  ps.account_intid
, ps.commodity_guid
, (reporting.fnt_nearestcommodityprice(ps.commodity_guid, enddate::date, -2)).price
, (reporting.fnt_nearestcommodityprice(ps.commodity_guid, enddate::date, -2)).pricedate
, ps.account
, ps.lot_balance
, ps.basis
, ps.value
, ps.funds_in
, ps.funds_out
, ps.realized_gain
, ps.unrealized_gain
, ps.total_gain
, ps.total_gain / ps.funds_in as rate_of_gain
, ps.income
, ps.brokerage_fees
, total_return
, ps.total_return / ps.funds_in as rate_of_return

from cte_portfoliosummary ps

order by
	ps.account_intid

$BODY$

LANGUAGE sql VOLATILE
COST 100
ROWS 1000;
