
-- apply to the gnucash application database to allow
-- use gnucash_reporting select privileges on base gnucash tables

CREATE USER gnucash_reporting WITH
  LOGIN
  NOSUPERUSER
  INHERIT
  NOCREATEDB
  NOCREATEROLE
  NOREPLICATION
  CONNECTION LIMIT 5;

 GRANT SELECT ON TABLE public.accounts TO gnucash_reporting;
 GRANT SELECT ON TABLE public.commodities TO gnucash_reporting;
 GRANT SELECT ON TABLE public.prices TO gnucash_reporting;
 GRANT SELECT ON TABLE public.splits TO gnucash_reporting;
 GRANT SELECT ON TABLE public.transactions TO gnucash_reporting;
