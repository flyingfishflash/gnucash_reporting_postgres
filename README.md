# Gnucash Reporting (Postgres)

A reporting environment for the Gnucash personal finance application. PostgreSQL is used as the data layer. A collection of Grafana dashboards is included as the presentation layer.

| Main         | Development |
| ------------ | ----------- |
| [![Build Status (main)](https://drone.flyingfishflash.net/api/badges/flyingfishflash/gnucash-reporting/status.svg)](https://drone.flyingfishflash.net/flyingfishflash/gnucash-reporting)| [![Build Status (development)](https://drone.flyingfishflash.net/api/badges/flyingfishflash/gnucash-reporting/status.svg?ref=refs/heads/development)](https://drone.flyingfishflash.net/flyingfishflash/gnucash-reporting)|



# Prerequisites

* Ensure the Postgres instance hosting your Gnucash application database has a role available for the foreign data wrapper in the gnucash-reporting container to use.

* The role name and password should be used for the FDW_USER and FDW_PASSWORD environment variables in the docker-compose.yml file below.

* Example script for creating the user: [add_role_gnucash_reporting.sql](https://gitlab.com/flyingfishflash/gnucash-reporting/blob/master/sql/maintenance/add_role_gnucash_reporting.sql)

# Usage

* Establish the the role to be used by the gnucash-reporting container to connect to your Gnucash application database

* Make a copy of the docker composition below

* Update the FDW* environment variables appropriately.

* Raise the container stack with *docker-compose -f docker-compose.yml up -d* or similar

* Postgres is available at: *postgres://gnucash_reporting@__your_docker_host__:53001/gnucash_reporting*  

# Caveats

Assumptions:

* The structure of accounts of the first *three* levels are used for grouping/categorization purposes, and *do not* contain any transactions. (eg: Assets:Financial Assets:401k, Assets:Financial Assets:Cash, etc)

* The account code field is numbered so that when sorting the account tree by code, the account hierarchy is represented in the proper order. This can be accomplished via Edit>Renumber Subaccounts within Gnucash.

Not tested with:

* Trading accounts

# docker-compose.yml:  

```yaml
version: "3.9"

services:

 gnucash-reporting:
  environment:
   - POSTGRES_PASSWORD=postgres
   - FDW_HOST=your_gnucash_app_db_hostname
   - FDW_DATABASE=your_gnucash_app_db_name
   - FDW_USER=your_gnucash_app_db_username
   - FDW_PASSWORD=your_gnucash_app_db_password
  image: registry.gitlab.com/flyingfishflash/gnucash-reporting:latest
  ports:
   - 53001:5432
  restart: always
 
```
# podman:  
```sh
#!/bin/sh

# generates, enables and starts a systemd user service 'pod-gnucash-reporting.service'

podman pod inspect gnucash-reporting || podman pod create --name=gnucash-reporting
podman network inspect gnucash-reporting || podman network create gnucash-reporting

podman create \
 --pod=gnucash-reporting \
 --name=gnucash-reporting-postgresql \
 -e POSTGRES_PASSWORD=postgres \
 -e FDW_HOST=dbhost.example.net \
 -e FDW_DATABASE=gnucash \
 -e FDW_USER=gnucash_reporting \
 -e FDW_PASSWORD=gnucash_reporting \
 --label io.containers.autoupdate=registry \
 --net gnucash-reporting \
 --network-alias gnucash-reporting-postgresql \
 --log-driver=journald \
 --log-opt=tag=podman/gnucash-reporting-postgresql \
 -p 53001:5432 \
 --pull=newer \
 --restart no \
 registry.gitlab.com/flyingfishflash/gnucash-reporting:latest

podman generate systemd gnucash-reporting --name --new --files --stop-timeout=60 --restart-sec=20

mv *.service ~/.config/systemd/user/

systemctl --user daemon-reload
systemctl --user enable pod-gnucash-reporting.service
systemctl --user restart pod-gnucash-reporting.service

```